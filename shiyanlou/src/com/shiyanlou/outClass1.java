package com.shiyanlou;

public class outClass1 {
	private String name = "wmsj100";

	public class Student {
		private int age = 10;

		public void stuInfo() {
			System.out.println(this.age + " " + outClass1.this.name);
		}
	}

	static String ID = "20152135";

	public static class Std {
		String ID = "std123";

		public void stdInfo() {
			System.out.println(this.ID + " " + (new outClass1().name) + " " + outClass1.ID);
		}
	}

	public static void main(String[] args) {
		outClass1 people = new outClass1();
		Std std1 = new Std();
		std1.stdInfo();
//		Student stu1 = people.new Student();
//		stu1.stuInfo();
	}
}
