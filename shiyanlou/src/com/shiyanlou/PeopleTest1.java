package com.shiyanlou;

public class PeopleTest1 {
	private int height = 13;

	public void setHeight(int hei) {
		this.height = hei;
	}

	public void peopleInfo() {
		final String sex = "male";
		class Student {
			String ID = "qwer";

			public void print() {
				System.out.println(this.ID + "  " + sex);
			}
		}
		Student a = new Student();
		a.print();

	}

	public void peopleInfo2(boolean b) {
		if (b) {
			final String sex = "male";
			class Student1 {
				String ID = "ssssss";

				public void print() {
					System.out.println(this.ID + "  " + sex);
				}
			}
			Student1 a2 = new Student1();
			a2.print();
		}
	}

	public Inner getInner(final String name, String city) {
		return new Inner() {
			private String nameStr = name;
			private String cityStr = city;

			public String getName() {
				return nameStr;
			}
		};
	}

	public static void main(String[] args) {
		PeopleTest1 aa = new PeopleTest1();
		aa.peopleInfo2(true);
		Inner inner = aa.getInner("wmsj100", "xian");
		System.out.println(inner.getName());
	}

	public int getHeight() {
		return this.height;
	}
}

interface Inner {
	String getName();
}
