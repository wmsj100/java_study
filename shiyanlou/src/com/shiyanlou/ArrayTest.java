// package com.shiyanlou;

public class ArrayTest {
	public static void main(String[] args) {
		int ages[] = { 1, 2, 3 };
		int a1[];
		a1 = ages;
		System.out.println(a1 == ages);
		for (int i = 0; i < ages.length; i++) {
			ages[i]++;
			System.out.println(ages[i]);
		}
		System.out.print(a1[0]);

		for (int age : ages) {
			System.out.println(age);
		}

		String[][] name = { { "zha", "haha", "jaja" }, { "zha", "haha", "jaja" }, { "zha", "haha", "jaja" }, };
		for (int i = 0; i < name.length; i++) {
			for (int j = 0; j < name[i].length; j++) {
				System.out.println(name[i][j]);
			}
		}
		System.out.println(name[0]);
		for (int i = 1; i <= 10; i++) {
			int x = (i - 1) * 10 + 1;
			for (int j = x; j < x + 10; j++) {
				System.out.printf("%5d", j);
			}
			System.out.println();
		}
		int [][] nums = new int[10][10];
		int count = 0;
		for(int i=0;i<10; i++) {
			for(int j=0;j<10;j++) {
				nums[i][j] = ++count;
				System.out.printf("%5d", count);
			}
			System.out.println();
		}
	}
}
