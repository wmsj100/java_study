package com.shiyanlou;

public class Test {
	public static void main(String[] args) {
		String str = (3 > 2) ? "isTrue" : "isFalse";
		boolean state = false || 1 < 2;
		boolean a = state;
		System.out.print(str);
		int days = 30;
		String msg;
		if (days > 20) {
			msg = "big";
		} else if (days > 10) {
			msg = "middls";
		} else {
			msg = "small";
		}
		int score = 78;
		if (score > 90) {
			msg = "优秀";
		} else if (score > 80) {
			msg = "良好";
		} else if (score > 60) {
			msg = "及格";
		} else {
			msg = "不及格";
		}
		System.out.println(msg);
		score = 1;
		switch (score) {
		case 1:
			msg = "first";
			break;
		case 2:
			msg = "second";
			break;
		default:
			msg = "sorry";
		}
		System.out.print(msg);

		int i = 1, j = 0, l = 0, k = 0;
		while (i <= 1000) {
			if (i % 2 == 0) {
				j += i;
			}
			i++;
		}
		System.out.print(j);
		do {
			if (l % 2 == 0) {
				k += l;
			}
			l++;
		} while (l <= 1000);
		System.out.print(k);

	}
}
