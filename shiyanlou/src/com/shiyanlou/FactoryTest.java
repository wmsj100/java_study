package com.shiyanlou;

public interface People1 {
	public void eat();

	public void sleep();

}

public class Male extends People1 {
	public void eat() {
		System.out.println("Male eat");
	}

	public void sleep() {
		System.out.println("Male sleep");
	}
}

public class Female extends People1 {
	public void eat() {
		System.out.println("Female eat");
	}

	public void sleep() {
		System.out.println("Female sleep");
	}
}

public class FactoryT {
	public People1 createPeo(String gender) {
		if (gender.equals("male")) {
			return new Male();
		} else if (gender.equals("female")) {
			return new Female();
		} else {
			System.out.println("Please input right gender");
		}
	}
}

public class TestFactory {
	public static void main(String[] args) {
		Factoryt test1 = new Factoryt();
		test1.createPeo("male");
		test1.eat();
		test1.sleep();
	}
}
