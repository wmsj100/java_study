package com.shiyanlou;
interface Animal{
	int y = 5;
	public void eat();
	public void travel();
}
public class Interface1 implements Animal{
	public void eat() {
		System.out.println("eat...");
	}
	
	public void travel() {
		System.out.println("travel...");
	}
	
	public static void main(String[] args) {
		Interface1 aa = new Interface1();
		aa.eat();
		aa.travel();
	}
}
