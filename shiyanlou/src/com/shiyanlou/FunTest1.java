package com.shiyanlou;
import com.shiyanlou.Dog;

public class FunTest1 {
	public static void main(String[] args) {
		Dog dog1 = new Dog();
		dog1.bark();
		FunTest1 a1 = new FunTest1();
		int a2 = a1.calSun(12, 15);
		a1.PrintName("wmsj100");
		System.out.println(a2);
		double a4 = a1.PrintAvg(11, 16);
		System.out.println(a4);
		a1.Fn1("heoo");
		a1.Fn1(1);
		a1.Fn1(1.3);
		a1.PrintTrigle(10);
	}

	public int calSun(int a, int b) {
		return a + b;
	}

	public void PrintName(String name) {
		System.out.println(name);
	}

	public double PrintAvg(int a, int b) {
		double avg = (a + b) / 2;
		return avg;
	}

	public void Fn1(int a) {
		System.out.println("int = " + a);
	}

	public void Fn1(String a) {
		System.out.println("string " + a);
	}

	public void Fn1(double a) {
		System.out.println("double " + a);
	}

	public int PrintTrigle(int n) {
		int count = 0;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= i; j++) {
				String a = "";
				count++;
				if (count < 10) {
					a = "0" + count;
				} else {
					a = "" + count;
				}
//				System.out.printf("%02d "+ count);
				System.out.printf("%02d ", count);
			}
			System.out.println();
		}
		return count;
	}
}
