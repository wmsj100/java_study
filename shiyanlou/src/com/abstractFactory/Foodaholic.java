package com.abstractFactory;

public class Foodaholic {
	public void eat(KitchenFactory a) {
		System.out.println(a.getFood().getFoodName() + "  " + a.getTableWare().getToolName());
	}

	public static void main(String[] args) {
		Foodaholic a1 = new Foodaholic();
		KitchenFactory a2 = new AKitchen();
		a1.eat(a2);
	}
}
