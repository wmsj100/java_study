package com.abstractFactory;

public interface KitchenFactory {
	public Food getFood();
	public TableWare getTableWare();
}
