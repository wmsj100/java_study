package com.abstractFactory;

public interface TableWare {
	public String getToolName();
}
