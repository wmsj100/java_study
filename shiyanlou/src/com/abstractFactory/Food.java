package com.abstractFactory;

public interface Food {
	public String getFoodName();
}
