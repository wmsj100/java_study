package com.factory;

public class TestFactory1 {
	public static void main(String[] args) {
		Human a5 = FactoryTest.createMale();
		a5.eat();
		a5.sleep();
		FactoryTest aa = new FactoryTest();
		Male a4 = aa.createMale();
		a4.eat();
		a4.sleep();
		Human a3 = aa.createPeople("female");
		a3.eat();
		a3.sleep();
	}
}
