package com.factory;

public class FactoryTest {
	public Human createPeople(String gender) {
		if (gender.equals("male")) {
			return new Male();
		} else if (gender.equals("female")) {
			return new Female();
		} else {
			System.out.println("please input right");
			return null;
		}
	}
	
	public static Male createMale() {
		return new Male();
	}
	
	public static Female createFemale() {
		return new Female();
	}
}
