package com.factory;

public class Female implements Human {
	public void eat() {
		System.out.println("Female eat...");
	}

	public void sleep() {
		System.out.println("Female sleep...");
	}
}
