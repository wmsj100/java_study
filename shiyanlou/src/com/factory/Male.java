package com.factory;

public class Male implements Human {
	public void eat() {
		System.out.println("Male eat...");
	}

	public void sleep() {
		System.out.println("Male sleep...");
	}
}
