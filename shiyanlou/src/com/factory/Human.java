package com.factory;

public interface Human {
	public void eat();

	public void sleep();
}
